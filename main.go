package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/logrusorgru/aurora/v3"
	"github.com/vbauerster/mpb/v7"
	"github.com/vbauerster/mpb/v7/decor"
	cliCfg "gitlab.com/isard/isardvdi-cli/cfg"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
)

// FUTURE: TODO: Deployments?

func readFromTerm(name string) (string, error) {
	fmt.Print(name + ": ")
	r := bufio.NewReader(os.Stdin)
	res, err := r.ReadString('\n')
	if err != nil {
		return "", fmt.Errorf("read %s from stdin: %w", name, err)
	}

	return strings.TrimSpace(res), nil
}

func waitForDesktopState(ctx context.Context, cli *client.Client, id, state string) error {
	tick := time.Tick(time.Second)
	after := time.After(time.Minute)

	for {
		select {
		case <-after:
			return errors.New("timeout")
		case <-tick:
			d, err := cli.DesktopGet(ctx, id)
			if err != nil {
				return err
			}

			if client.GetString(d.State) == "Failed" {
				return errors.New("desktop failed")
			}

			if client.GetString(d.State) == state {
				return nil
			}
		}
	}
}

type WorkerState int

const (
	WorkerStateUnknown = iota
	WorkerStateCreatingUser
	WorkerStateCreatingDesktop
	WorkerStateStartingDesktop
	WorkerStateStartingViewer
	WorkerStateGeneratingLoad
	WorkerStateStoppingViewer
	WorkerStateStoppingDesktop
	WorkerStateDeletingDesktop
	WorkerStateDeletingUser
	WorkerStateDone
)

func (w WorkerState) String() string {
	switch w {
	case WorkerStateCreatingUser:
		return "creating user"
	case WorkerStateCreatingDesktop:
		return "creating desktop"
	case WorkerStateStartingDesktop:
		return "starting desktop"
	case WorkerStateStartingViewer:
		return "starting viewer"
	case WorkerStateGeneratingLoad:
		return "generating desktop load"
	case WorkerStateStoppingViewer:
		return "stopping viewer"
	case WorkerStateStoppingDesktop:
		return "stopping desktop"
	case WorkerStateDeletingDesktop:
		return "deleting desktop"
	case WorkerStateDeletingUser:
		return "deleting user"
	case WorkerStateDone:
		return aurora.Green("✓").String()

	default:
		return "Unknown state"
	}
}

func main() {
	ctx := context.Background()

	cfg := NewCfg("")

	cli, err := client.NewClient(&cliCfg.Cfg{
		Host: cfg.Host,
	})
	if err != nil {
		panic(err)
	}
	cli.UserAgent = cli.UserAgent + " + isardvdi-lamborghini v0.1.0"

	tkn, err := cli.AuthForm(ctx, cfg.AdminCategory, cfg.AdminUser, cfg.AdminPassword)
	if err != nil {
		panic(err)
	}
	cli.Token = tkn

	// Ensure user is admin

	// TODO: Set concurrent workers number

	var wg sync.WaitGroup
	p := mpb.New(mpb.WithWaitGroup(&wg))

	for i := 0; i < cfg.Concurrency; i++ {
		wg.Add(1)

		go func(id int) {
			defer wg.Done()

			barName := fmt.Sprintf("User %d:", id)

			errMsg := ""
			var errState WorkerState = WorkerStateUnknown
			var state WorkerState = WorkerStateCreatingUser
			bar := p.New(100,
				mpb.SpinnerStyle(),
				mpb.BarWidth(1),
				mpb.AppendDecorators(
					decor.Name(barName, decor.WC{W: len(barName) + 1, C: decor.DidentRight}),
					decor.Any(func(s decor.Statistics) string {
						if s.Aborted {
							return aurora.Red("✗").String() + "  " + errState.String() + ": " + errMsg
						}

						return state.String()
					}),
				),
			)

			defer func() {
				state = WorkerStateDone
				bar.SetCurrent(100)
			}()

			username := fmt.Sprintf("isardvdi_lamborghini_%d", id)
			u, err := cli.AdminUserCreate(ctx, cfg.UsersProvider, cfg.UsersRole, cfg.UsersCategory, cfg.UsersGroup, username, username, username)
			if err != nil {
				errMsg = err.Error()
				bar.Abort(false)
				return
			}

			defer func() {
				state = WorkerStateDeletingUser
				if err := cli.AdminUserDelete(ctx, client.GetString(u.ID)); err != nil {
					errMsg = err.Error()
					errState = state
					bar.Abort(false)
					return
				}
			}()

			wkrCli, err := client.NewClient(&cliCfg.Cfg{
				Host: cfg.Host,
			})
			if err != nil {
				errMsg = err.Error()
				errState = state
				bar.Abort(false)
				return
			}

			tkn, err := wkrCli.AuthForm(ctx, cfg.UsersCategory, username, username)
			if err != nil {
				errMsg = err.Error()
				errState = state
				bar.Abort(false)
				return
			}
			wkrCli.Token = tkn

			state = WorkerStateCreatingDesktop
			d, err := wkrCli.DesktopCreate(ctx, strings.ToTitle(strings.Join(strings.Split(username, "_"), " ")), cfg.DesktopTemplate)
			if err != nil {
				errMsg = err.Error()
				errState = state
				bar.Abort(false)
				return
			}

			defer func() {
				state = WorkerStateDeletingDesktop
				if err := wkrCli.DesktopDelete(ctx, client.GetString(d.ID)); err != nil {
					errMsg = err.Error()
					errState = state
					bar.Abort(false)
					return
				}
				// TODO: Wait for the desktop to be deleted
			}()

			if err := waitForDesktopState(ctx, wkrCli, client.GetString(d.ID), "Stopped"); err != nil {
				errMsg = err.Error()
				errState = state
				bar.Abort(false)
				return
			}

			state = WorkerStateStartingDesktop
			if err := wkrCli.DesktopStart(ctx, client.GetString(d.ID)); err != nil {
				errMsg = err.Error()
				errState = state
				bar.Abort(false)
				return
			}

			if err := waitForDesktopState(ctx, wkrCli, client.GetString(d.ID), "Started"); err != nil {
				errMsg = err.Error()
				errState = state
				bar.Abort(false)
				return
			}

			defer func() {
				state = WorkerStateStoppingDesktop

				if err := wkrCli.DesktopStop(ctx, client.GetString(d.ID)); err != nil {
					errMsg = err.Error()
					errState = state
					bar.Abort(false)
					return
				}

				if err := waitForDesktopState(ctx, wkrCli, client.GetString(d.ID), "Stopped"); err != nil {
					errMsg = err.Error()
					errState = state
					bar.Abort(false)
					return
				}
			}()

			// Start viewers
			state = WorkerStateStartingViewer
			time.Sleep(5 * time.Second)

			// Connect to the desktops and generate load
			state = WorkerStateGeneratingLoad
			time.Sleep(10 * time.Second)

			// Stop the viewers
			state = WorkerStateStoppingDesktop
			time.Sleep(5 * time.Second)
		}(i + 1)
	}

	p.Wait()
	wg.Wait()

	// Load testing:
	// Collect all the stats and generate graphics? (we could simply use the grafana panels)

	fmt.Println("\n")
	fmt.Println("IsardVDI Lamborghini finished!!")
	fmt.Println("\n")
	fmt.Println(`                              _.-="_-         _
                         _.-="   _-          | ||"""""""---._______     __..
             ___.===""""-.______-,,,,,,,,,,,,` + "`" + `-''----" """""       """""  __'
      __.--""     __        ,'                   o \           __        [__|
 __-""=======.--""  ""--.=================================.--""  ""--.=======:
]       [w] : /        \ : |========================|    : /        \ :  [w] :
V___________:|          |: |========================|    :|          |:   _-"
 V__________: \        / :_|=======================/_____: \        / :__-"
 -----------'  "-____-"  ` + "`" + `-------------------------------'  "-____-"`)
	fmt.Println("\n")
	fmt.Println("FIAAAAAAAAAAAAAAAAU")
}
