package main

import (
	"errors"
	"log"
	"os"
	"strings"

	"github.com/spf13/viper"
)

type Cfg struct {
	Host            string
	Concurrency     int
	DesktopTemplate string `mapstructure:"desktop_template"`

	AdminCategory string `mapstructure:"admin_category"`
	AdminUser     string `mapstructure:"admin_user"`
	AdminPassword string `mapstructure:"admin_password"`

	UsersProvider string `mapstructure:"users_provider"`
	UsersRole     string `mapstructure:"users_role"`
	UsersCategory string `mapstructure:"users_category"`
	UsersGroup    string `mapstructure:"users_group"`
}

func NewCfg(name string) *Cfg {
	if name == "" {
		name = "isardvdi-lamborghini"
		os.Create(name + ".toml")
	}

	viper.SetConfigName(strings.ToLower(name))

	viper.AddConfigPath(".")
	viper.AddConfigPath("$HOME/.isardvdi")
	viper.AddConfigPath("$HOME/.config/isardvdi")
	viper.AddConfigPath("/etc/isardvdi")

	viper.SetDefault("host", "https://localhost")
	viper.SetDefault("concurrency", 10)
	viper.SetDefault("desktop_template", "_local-default-admin-admin-Template_Slax_9.3.0")
	viper.SetDefault("admin_category", "default")
	viper.SetDefault("admin_user", "admin")
	viper.SetDefault("admin_password", "IsardVDI")
	viper.SetDefault("users_provider", "local")
	viper.SetDefault("users_role", "user")
	viper.SetDefault("users_category", "default")
	viper.SetDefault("users_group", "default-default")

	viper.SetEnvPrefix(strings.ToUpper(name))
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		if !errors.As(err, &viper.ConfigFileNotFoundError{}) {
			log.Fatalf("read configuration: %v", err)
		}

		log.Println("Configuration file not found, using environment variables and defaults")
	}

	cfg := &Cfg{}
	if err := viper.Unmarshal(cfg); err != nil {
		log.Fatalf("unmarshal configuration: %v", err)
	}

	if err := viper.WriteConfig(); err != nil {
		log.Fatalf("write test configuration: %v", err)
	}

	return cfg
}
